# LibDIAL

LibDIAL is an implementation of the DIAL protocol (DIscovery And Launch) created
by Google and Netflix with influence and input from vendors.

## Development Requirements

To build LibDIAL you'll need [Bazel](http://bazel.io) installed
to build the code. This is in turn requires Java. Bazel can
handle dependencies from there.

## Compiling

To compile the C library simple run this command:

    bazel build //dial

## Testing

To run tests on the C library run this command:

    bazel test //dial:dial-test

