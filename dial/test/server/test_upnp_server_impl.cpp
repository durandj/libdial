#include <fff.h>
#include <gtest/gtest.h>
#include <upnp.h>

#include "server/upnp/upnp_errors.hpp"
#include "server/upnp/upnp_server_impl.hpp"

using namespace dial::server;
using namespace fruit;

namespace {

DEFINE_FFF_GLOBALS;
FAKE_VALUE_FUNC(int, UpnpInit, const char *, unsigned short);
FAKE_VALUE_FUNC(int, UpnpFinish);
FAKE_VALUE_FUNC(char *, UpnpGetServerIpAddress);
FAKE_VALUE_FUNC(unsigned short, UpnpGetServerPort);

auto get_upnp_server_factory(void) {
    Injector<UpnpServerFactory> injector(getUpnpServerImplComponent());

    return UpnpServerFactory(injector);
}

auto get_upnp_server(const char *host = nullptr, int port = 0) {
    UpnpServerFactory factory = get_upnp_server_factory();

    return factory(host, port);
}

TEST(upnpServerImpl, getHost) {
    auto upnp_server = get_upnp_server("localhost");

    EXPECT_STREQ("localhost", upnp_server->getHost());
}

TEST(upnpServerImpl, getHostNullptr) {
    RESET_FAKE(UpnpGetServerIpAddress);

    UpnpGetServerIpAddress_fake.return_val = (char *) "127.0.0.1";

    auto upnp_server = get_upnp_server();
    auto host = upnp_server->getHost();

    EXPECT_EQ(1, UpnpGetServerIpAddress_fake.call_count);
    EXPECT_STREQ("127.0.0.1", host);
}

TEST(upnpServerImpl, getPort) {
    auto upnp_server = get_upnp_server(nullptr, 1900);

    EXPECT_EQ(1900, upnp_server->getPort());
}

TEST(upnpServerImpl, getPortZero) {
    RESET_FAKE(UpnpGetServerPort);

    UpnpGetServerPort_fake.return_val = 1900;

    auto upnp_server = get_upnp_server();
    auto port = upnp_server->getPort();

    EXPECT_EQ(1, UpnpGetServerPort_fake.call_count);
    EXPECT_EQ(1900, port);
}

TEST(upnpServerImpl, start) {
    RESET_FAKE(UpnpInit);

    UpnpInit_fake.return_val = UPNP_E_SUCCESS;

    auto upnp_server = get_upnp_server();
    upnp_server->start();

    EXPECT_EQ(1, UpnpInit_fake.call_count);
}

template <int ErrorCode, typename ExceptionType>
void check_upnp_start_exception(void) {
    RESET_FAKE(UpnpInit);

    UpnpInit_fake.return_val = ErrorCode;

    EXPECT_THROW({
        auto upnp_server = get_upnp_server();
        upnp_server->start();
    }, ExceptionType);
}

TEST(upnpServerImpl, startOutOfMemoryError) {
    check_upnp_start_exception<UPNP_E_OUTOF_MEMORY, UpnpOutOfMemoryException>();
}

TEST(upnpServerImpl, startInitError) {
    check_upnp_start_exception<UPNP_E_INIT, UpnpInitializedException>();
}

TEST(upnpServerImpl, startInitFailedError) {
    check_upnp_start_exception<UPNP_E_INIT_FAILED, UpnpInitFailedException>();
}

TEST(upnpServerImpl, startSocketError) {
    check_upnp_start_exception<UPNP_E_SOCKET_BIND, UpnpSocketBindException>();
}

TEST(upnpServerImpl, startListenError) {
    check_upnp_start_exception<UPNP_E_LISTEN, UpnpListenException>();
}

TEST(upnpServerImpl, startOutOfSocketError) {
    check_upnp_start_exception<UPNP_E_OUTOF_SOCKET, UpnpOutOfSocketException>();
}

TEST(upnpServerImpl, startInternalError) {
    check_upnp_start_exception<UPNP_E_INTERNAL_ERROR, UpnpInternalException>();
}

TEST(upnpServerImpl, stop) {
    RESET_FAKE(UpnpFinish);

    UpnpFinish_fake.return_val = UPNP_E_SUCCESS;

    auto upnp_server = get_upnp_server();
    upnp_server->stop();

    EXPECT_EQ(1, UpnpFinish_fake.call_count);
}

TEST(upnpServerImpl, stopFinishError) {
    RESET_FAKE(UpnpFinish);

    UpnpFinish_fake.return_val = UPNP_E_FINISH;

    EXPECT_THROW({
        auto upnp_server = get_upnp_server();
        upnp_server->stop();
    }, UpnpFinishException);
}

} // namespace

