#include <gtest/gtest.h>

#include <dial/server/server.h>

namespace {
    TEST(dialServerAPI, constructor) {
        dial_error_t *err = nullptr;
        dial_server_t server = dial_server_construct("127.0.0.1", 1900, err);

        ASSERT_EQ(nullptr, err);
        ASSERT_TRUE(server != nullptr);

        dial_server_destruct(server);
    }
} // namespace

