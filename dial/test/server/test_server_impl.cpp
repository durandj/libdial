#include <gtest/gtest.h>

#include "mocks/mock_upnp_server.hpp"
#include "server/server_impl.hpp"

namespace {

using namespace dial::server;
using namespace fruit;

Component<DialServer> getTestableDialServerComponent(UpnpServer *upnp_server) {
    return createComponent()
        .install(getDialServerImplComponent())
        .bindInstance(*upnp_server);
}

TEST(dialServerImpl, startUpnpServer) {
    MockUpnpServer upnp_server;
    EXPECT_CALL(upnp_server, start());

    Injector<DialServer> injector = getTestableDialServerComponent((UpnpServer *) &upnp_server);
    DialServer *server = injector.get<DialServer *>();

    server->startUpnpServer();
}

TEST(dialServerImpl, stopUpnpServer) {
    MockUpnpServer upnp_server;
    EXPECT_CALL(upnp_server, stop());

    Injector<DialServer> injector = getTestableDialServerComponent((UpnpServer *) &upnp_server);
    DialServer *server = injector.get<DialServer *>();

    server->stopUpnpServer();
}

} // namespace

