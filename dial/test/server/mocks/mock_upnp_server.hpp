#ifndef TEST_SERVER_MOCKS_UPNP_SERVER_HPP_
# define TEST_SERVER_MOCKS_UPNP_SERVER_HPP_

# include <fruit/fruit.h>
# include <gmock/gmock.h>

# include "server/upnp/upnp_server.hpp"

namespace dial {
namespace server {

class MockUpnpServer : public UpnpServer {
public:
    MockUpnpServer(void) = default;
    MOCK_METHOD0(start, void(void));
    MOCK_METHOD0(stop, void(void));
    MOCK_METHOD0(getHost, const char *(void));
    MOCK_METHOD0(getPort, int(void));
};

} // namespace server
} // namespace dial

#endif

