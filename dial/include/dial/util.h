#ifndef DIAL_UTIL_H_
# define DIAL_UTIL_H_

# if defined(_WIN32) || defined(__CYGWIN__)
#  ifdef DIAL_EXPORTS
#   ifdef __GNUC__
#    define DIAL_EXPORT __attribute__((dllexport))
#   else
#    define DIAL_EXPORT __declspec(dllexport)
#   endif
#  else
#   ifdef __GNUC__
#    define DIAL_EXPORT __attribute__((dllimport))
#   else
#    define DIAL_EXPORT __declspec(dllimport)
#   endif
#  endif
# else
#  if __GNUC__ >= 4
#   define DIAL_EXPORT __attribute__((visibility ("default")))
#  else
#   define DIAL_EXPORT
#  endif
# endif

# define IN
# define OUT

#endif

