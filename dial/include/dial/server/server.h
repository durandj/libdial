#ifndef DIAL_SERVER_SERVER_H_
# define DIAL_SERVER_SERVER_H_

#include <dial/util.h>

# ifdef __cplusplus
extern "C" {
# endif

typedef struct dial_error *dial_error_t;

DIAL_EXPORT
const char *dial_error_message(IN dial_error_t error);

DIAL_EXPORT
void dial_error_destruct(IN dial_error_t error);

typedef struct dial_server *dial_server_t;

DIAL_EXPORT
dial_server_t dial_server_construct(
    IN const char *host,
    IN int upnp_port,
    OUT dial_error_t *err
);

DIAL_EXPORT
void dial_server_destruct(IN dial_server_t server);

# ifdef __cplusplus
} // extern "C"
# endif

#endif

