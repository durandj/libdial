#ifndef DIAL_SERVER_SERVER_HPP_
# define DIAL_SERVER_SERVER_HPP_

# include <fruit/fruit.h>

# include "upnp/upnp_server.hpp"

namespace dial {
namespace server {

class DialServer {
public:
    virtual void startUpnpServer(void) = 0;
    virtual void stopUpnpServer(void) = 0;
};

} // namespace server
} // namespace dial

#endif

