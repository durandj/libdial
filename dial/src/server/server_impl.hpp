#ifndef DIAL_SERVER_SERVER_IMPL_HPP_
# define DIAL_SERVER_SERVER_IMPL_HPP_

# include <fruit/fruit.h>

# include "server.hpp"
# include "upnp/upnp_server.hpp"

namespace dial {
namespace server {

fruit::Component<fruit::Required<UpnpServer>, DialServer>
getDialServerImplComponent(void);

} // namespace server
} // namespace dial

#endif

