#include "upnp_server_impl.hpp"

#include <upnp.h>

#include "upnp_errors.hpp"

using namespace fruit;

namespace dial {
namespace server {

class UpnpServerImpl : public UpnpServer {
private:
    const char *host_;
    int port_;

public:
    INJECT(UpnpServerImpl(
            ASSISTED(const char *) host,
            ASSISTED(int) port)) :
        host_(host), port_(port) {
    }

    virtual void start(void) override {
        auto result = UpnpInit(host_, port_);
        if (result != UPNP_E_SUCCESS) {
            raise_upnp_exception(result);
        }
    }

    virtual void stop(void) override {
        auto result = UpnpFinish();
        if (result != UPNP_E_SUCCESS) {
            raise_upnp_exception(result);
        }
    }

    virtual const char *getHost(void) override {
        if (host_ == nullptr) {
            host_ = UpnpGetServerIpAddress();
        }

        return host_;
    }

    virtual int getPort(void) override {
        if (port_ <= 0) {
            port_ = UpnpGetServerPort();
        }

        return port_;
    }
};

Component<UpnpServerFactory> getUpnpServerImplComponent(void) {
    return createComponent()
        .bind<UpnpServer, UpnpServerImpl>()
        .registerFactory<std::unique_ptr<UpnpServer>(Assisted<const char *>, Assisted<int>)>(
            [](const char *host, int port) {
                return std::unique_ptr<UpnpServer>(new UpnpServerImpl(host, port));
            }
        );
}

} // namespace server
} // namespace dial

