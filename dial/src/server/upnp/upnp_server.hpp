#ifndef DIAL_SERVER_UPNP_SERVER_HPP_
# define DIAL_SERVER_UPNP_SERVER_HPP_

namespace dial {
namespace server {

class UpnpServer {
public:
    virtual void start(void) = 0;
    virtual void stop(void) = 0;

    virtual const char *getHost(void) = 0;
    virtual int getPort(void) = 0;
};

} // namespace server
} // namespace dial

#endif

