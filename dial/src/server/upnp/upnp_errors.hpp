#ifndef DIAL_SERVER_UPNP_ERRORS_HPP_
# define DIAL_SERVER_UPNP_ERRORS_HPP_

# include <upnp.h>

# include <stdexcept>

namespace dial {
namespace server {

class UpnpException : public std::runtime_error {
public:
    explicit UpnpException(const char *what_arg);
};

class UpnpOutOfMemoryException : public UpnpException {
public:
    UpnpOutOfMemoryException(void);
};

class UpnpInitializedException : public UpnpException {
public:
    UpnpInitializedException(void);
};

class UpnpInitFailedException : public UpnpException {
public:
    UpnpInitFailedException(void);
};

class UpnpSocketBindException : public UpnpException {
public:
    UpnpSocketBindException(void);
};

class UpnpListenException : public UpnpException {
public:
    UpnpListenException(void);
};

class UpnpOutOfSocketException : public UpnpException {
public:
    UpnpOutOfSocketException(void);
};

class UpnpInternalException : public UpnpException {
public:
    UpnpInternalException(void);
};

class UpnpFinishException : public UpnpException {
public:
    UpnpFinishException(void);
};

void raise_upnp_exception(int condition);

} // namespace server
} // namespace dial

#endif

