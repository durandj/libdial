#ifndef DIAL_SERVER_UPNP_SERVER_IMPL_HPP_
# define DIAL_SERVER_UPNP_SERVER_IMPL_HPP_

# include <fruit/fruit.h>

# include <functional> // std::function
# include <memory>     // std::unique_ptr

# include "upnp_server.hpp"

namespace dial {
namespace server {

using UpnpServerFactory = std::function<std::unique_ptr<UpnpServer>(const char *, int)>;

fruit::Component<UpnpServerFactory> getUpnpServerImplComponent(void);

} // namespace server
} // namespace dial

#endif

