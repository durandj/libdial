#include "upnp_errors.hpp"

namespace dial {
namespace server {

UpnpException::UpnpException(const char *what_arg) :
    std::runtime_error(what_arg) {
}

UpnpOutOfMemoryException::UpnpOutOfMemoryException(void) :
    UpnpException("UPNP out of memory") {
}

UpnpInitializedException::UpnpInitializedException(void) :
    UpnpException("UPNP already initialized") {
}

UpnpInitFailedException::UpnpInitFailedException(void) :
    UpnpException("Unknown UPNP initialization error") {
}

UpnpSocketBindException::UpnpSocketBindException(void) :
    UpnpException("Upnp was unable to a bind socket") {
}

UpnpListenException::UpnpListenException(void) :
    UpnpException("UPNP was unable to listen to a socket") {
}

UpnpOutOfSocketException::UpnpOutOfSocketException(void) :
    UpnpException("UPNP has no remaining sockets") {
}

UpnpInternalException::UpnpInternalException(void) :
    UpnpException("Internal UPNP exception") {
}

UpnpFinishException::UpnpFinishException(void) :
    UpnpException("UPNP is already stopped") {
}

void raise_upnp_exception(int condition) {
    switch (condition) {
    case UPNP_E_OUTOF_MEMORY:
        throw UpnpOutOfMemoryException();
    case UPNP_E_INIT:
        throw UpnpInitializedException();
    case UPNP_E_INIT_FAILED:
        throw UpnpInitFailedException();
    case UPNP_E_SOCKET_BIND:
        throw UpnpSocketBindException();
    case UPNP_E_LISTEN:
        throw UpnpListenException();
    case UPNP_E_OUTOF_SOCKET:
        throw UpnpOutOfSocketException();
    case UPNP_E_INTERNAL_ERROR:
        throw UpnpInternalException();
    case UPNP_E_FINISH:
        throw UpnpFinishException();
    default:
        throw UpnpException("Invalid UPNP exception");
    }
}

} // namespace server
} // namespace dial

