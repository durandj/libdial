#ifndef DIAL_SERVER_SERVER_API_ERROR_HPP_
# define DIAL_SERVER_SERVER_API_ERROR_HPP_

# include <dial/server/server.h>

#include <string> // std::string

extern "C" {

struct dial_error {
    std::string message;
};

} // extern "C"

#endif

