#include <dial/server/server.h>

#include "util.hpp"
#include "../server.hpp"
#include "../server_impl.hpp"
#include "../upnp/upnp_server_impl.hpp"

using namespace dial::server;
using namespace fruit;

Component<DialServer> get_dial_server_component(
        const char *host,
        int upnp_port) {
    Injector<UpnpServerFactory> upnp_injector(getUpnpServerImplComponent());
    UpnpServerFactory upnp_factory(upnp_injector);

    return createComponent()
        .install(getDialServerImplComponent())
        .bindInstance(*upnp_factory(host, upnp_port));
}

extern "C" {

struct dial_server {
    dial_server(DialServer *server) : instance(server) {
    }

    DialServer *instance;
};

dial_server_t dial_server_construct(
    IN const char *host,
    IN int upnp_port,
    OUT dial_error_t *err) {
    dial_server_t server = nullptr;

    translate_exceptions(
        err,
        [&] {
            Injector<DialServer> injector(get_dial_server_component(host, upnp_port));

            server = std::make_unique<dial_server>(
                (DialServer *) injector.get<DialServer *>()
            ).release();
        }
    );

    return server;
}

void dial_server_destruct(IN dial_server_t server) {
    delete server;
}

} // extern "C"

