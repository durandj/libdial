#ifndef DIAL_SERVER_SERVER_API_UTIL_HPP_
# define DIAL_SERVER_SERVER_API_UTIL_HPP_

# include <stdexcept> // std::exception

# include "error.hpp"

namespace dial {
namespace server {

template <typename Fn>
bool translate_exceptions(OUT dial_error_t *err, Fn &&fn) {
    try {
        fn();
    }
    catch (const std::exception &e) {
        *err = new dial_error{e.what()};

        return false;
    }
    catch (...) {
        *err = new dial_error{"Unknown internal error"};

        return false;
    }

    return true;
}

} // server
} // namespace dial

#endif

