#include "error.hpp"

extern "C" {

const char *dial_error_message(dial_error_t err) {
    return err->message.c_str();
}

void dial_error_destruct(dial_error_t err) {
    delete err;
}

} // extern "C"

