#include "server_impl.hpp"

using namespace fruit;

namespace dial {
namespace server {

class DialServerImpl : public DialServer {
private:
    UpnpServer *upnpServer_;

public:
    INJECT(DialServerImpl(UpnpServer *upnp_server)) :
        upnpServer_(upnp_server) {
    }

    virtual void startUpnpServer(void) override {
        upnpServer_->start();
    }

    virtual void stopUpnpServer(void) override {
        upnpServer_->stop();
    }
};

Component<Required<UpnpServer>, DialServer>
getDialServerImplComponent(void) {
    return createComponent()
        .bind<DialServer, DialServerImpl>();
}

} // namespace server
} // namespace dial

